//
//  ViewController.m
//  Game tictactoe
//
//  Created by Clicklabs 104 on 9/28/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIButton *h1;
UIButton *h2;
UIButton *h3;
UIButton *h4;
UIButton *h5;
UIButton *h6;
UIButton *h7;
UIButton *h8;
UIButton *h9;
UILabel *label;
UILabel *turn;
UIButton *reset;
NSInteger player;
- (void)viewDidLoad {
    [super viewDidLoad];
    /*UIImageView *image= [[UIImageView alloc] initWithFrame:CGRectMake(100, 150, 10, 300)];;
    image = [UIImage imageNamed:@"images-1.jpg"];
    [self.view addSubview:image];
    */
    label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(5,100,400,30)];
    label.textColor=[UIColor blueColor];
    label.font=[UIFont fontWithName:@"Chalkduster" size:30];
    label.text= @"Tic Tac Toe";
    label.textAlignment= NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    h1 = [[UIButton alloc]initWithFrame:CGRectMake(50,150,95,95)];
    h1.backgroundColor=[UIColor lightGrayColor];
    h1.tag =1;
    //[h1 setImage:[UIImage imageNamed:@"x.png"]forState:UIControlStateNormal];
    [h1 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h1];
    
    h2 = [[UIButton alloc]initWithFrame:CGRectMake(150,150,95,95)];
    h2.backgroundColor=[UIColor lightGrayColor];
    h2.tag = 2;
    [h2 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h2];
    
    h3 = [[UIButton alloc]initWithFrame:CGRectMake(250,150,95,95)];
    h3.backgroundColor=[UIColor lightGrayColor];
    h3.tag = 3;
    [h3 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h3];
    
    h4 = [[UIButton alloc]initWithFrame:CGRectMake(50,250,95,95)];
    h4.backgroundColor=[UIColor lightGrayColor];
    h4.tag =4;
    [h4 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h4];
    
    h5 = [[UIButton alloc]initWithFrame:CGRectMake(150,250,95,95)];
    h5.backgroundColor=[UIColor lightGrayColor];
    h5.tag=5;
    [h5 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h5];
    
    h6 = [[UIButton alloc]initWithFrame:CGRectMake(250,250,95,95)];
    h6.backgroundColor=[UIColor lightGrayColor];
    h6.tag=6;
    [h6 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h6];
    
    h7 = [[UIButton alloc]initWithFrame:CGRectMake(50,350,95,95)];
    h7.backgroundColor=[UIColor lightGrayColor];
    h7.tag=7;
    [h7 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h7];
    
    h8 = [[UIButton alloc]initWithFrame:CGRectMake(150,350,95,95)];
    h8.backgroundColor=[UIColor lightGrayColor];
    h8.tag=8;
    [h8 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h8];
    
    h9 = [[UIButton alloc]initWithFrame:CGRectMake(250,350,95,95)];
    h9.backgroundColor=[UIColor lightGrayColor];
    h9.tag=9;
    [h9 addTarget:self action: @selector(touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:h9];
    
    turn = [[UILabel alloc] init];
    [turn setFrame:CGRectMake(10,480,400,90)];
    turn.textColor=[UIColor blueColor];
    turn.font=[UIFont fontWithName:@"Chalkduster" size:30];
    turn.textAlignment= NSTextAlignmentCenter;
    [self.view addSubview:turn];

    
    reset = [[UIButton alloc]initWithFrame:CGRectMake(100,600,200,100)];
    [reset setTitle:@"Reset Game" forState:UIControlStateNormal];
   [reset setTitleColor: [UIColor greenColor] forState:(UIControlStateNormal)];
    [reset addTarget:self action: @selector(reset:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];

    player=1;
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)reset:(UIButton*) sender{
    h1=NULL;
    h2=NULL;
    h3= NULL;
    h4=NULL;
    h5= NULL;
    h6=NULL;
    h7=NULL;
    h8=NULL;
    h8=NULL;
    h9=NULL;
    player=1;
    turn.text=@"player 1 turn";
}


-(void)touch:(UIButton*)sender
{
    
    if([sender tag] == 1)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h1 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h1 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }

    }
    else if([sender tag] == 2)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h2 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h2 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }

    }
    else if([sender tag]==3)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h3 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h3 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }

    }
   else if([sender tag]==4)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h4 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h4 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }

    }
    else if([sender tag]==5)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h5 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h5 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }
    }
    else if([sender tag]==6)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h6 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h6 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }
    }
    else if([sender tag]==7)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h7 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h7 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }
        [self check];

        
    }
    else if([sender tag]==8)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h8 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h8 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }

    }
    else if([sender tag]==9)
    {
        if(player == 1) {
            player = 2;
            turn.text = @"player 2 turn";
            [h9 setImage:[UIImage imageNamed:@"x.jpg"]forState:UIControlStateNormal];
        }
        else if(player == 2) {
            player = 1;
            turn.text =@"player 1 turn";
            [h9 setImage:[UIImage imageNamed:@"o.jpg"]forState:UIControlStateNormal];
        }
        [self check];

    }
    else if ([self check]== YES)
    {
        if(player==1){
            turn.text=@"winner- player 1";
        }
        else{
            turn.text=@"winner- player 2";
        }
    }
}
-(BOOL) check{
     if ((h1.currentImage==h2.currentImage)&&(h2.currentImage == h3.currentImage)&&(h1.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h4.currentImage==h5.currentImage)&&(h5.currentImage == h6.currentImage)&&(h4.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h7.currentImage==h8.currentImage)&&(h8.currentImage == h9.currentImage)&&(h7.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h1.currentImage==h4.currentImage)&&(h4.currentImage == h7.currentImage)&&(h1.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h2.currentImage==h5.currentImage)&&(h5.currentImage == h8.currentImage)&&(h2.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h3.currentImage==h6.currentImage)&&(h6.currentImage == h9.currentImage)&&(h3.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h1.currentImage==h5.currentImage)&&(h5.currentImage == h9.currentImage)&&(h1.currentImage!=NULL))
    {
        return YES;
    }
    else if ((h3.currentImage==h5.currentImage)&&(h5.currentImage == h7.currentImage)&&(h3.currentImage!=NULL))
    {
        return YES;
    }
    return NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
